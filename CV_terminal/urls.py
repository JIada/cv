from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from CV.views import MainInfoViewSet, PhotoViewSet, WorkViewSet, StudyViewSet, ConsoleCommandsViewSet

router = routers.DefaultRouter()
router.register(r'main_info', MainInfoViewSet)
router.register(r'photo', PhotoViewSet)
router.register(r'study', StudyViewSet)
router.register(r'work', WorkViewSet)
router.register(r'console_commands', ConsoleCommandsViewSet)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^', include(router.urls)),
]
