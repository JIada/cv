from django.contrib import admin
from CV.models import (
    MainInfoModel, PhotoModel, WorkModel, StudyModel, ResponsibilityModel, ProgressModel, CompetenceModel,
    ConsoleCommands, LanguageModel
)


class CompetenceAdmin(admin.ModelAdmin):
    list_display = [
        'Competence',
    ]


class LanguageAdmin(admin.ModelAdmin):
    list_display = [
        'Description',
    ]


class MainInfoAdmin(admin.ModelAdmin):
    list_display = [
        'Name', 'Surname', 'Age', 'Email', 'Phone',
    ]


class PhotoAdmin(admin.ModelAdmin):
    list_display = [
        'Photo',
    ]


class WorkAdmin(admin.ModelAdmin):
    list_display = [
        'Organization', 'Start_date', 'End_date', 'Specialty',
    ]


class StudyAdmin(admin.ModelAdmin):
    list_display = [
        'Organization', 'Start_date', 'End_date', 'Position',
    ]


class ResponsibilityModelAdmin(admin.ModelAdmin):
    list_display = [
        'Responsibility',
    ]


class ProgressModelAdmin(admin.ModelAdmin):
    list_display = [
        'Progress', 'Start_date', 'End_date', 'Position',
    ]


class ConsoleModelAdmin(admin.ModelAdmin):
    list_display = [
        'Command', 'Description',
    ]

admin.site.register(
    [
        MainInfoModel, PhotoModel, WorkModel, StudyModel, ResponsibilityModel, ProgressModel, CompetenceModel,
        ConsoleCommands, LanguageModel
    ]
)
