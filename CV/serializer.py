from rest_framework import serializers
from CV.common import AllMessage
from CV.models import MainInfoModel, PhotoModel, StudyModel, WorkModel, ConsoleCommands


class CompetenceSerializer(serializers.Serializer):
    Competence_id = serializers.IntegerField(read_only=True)
    Description = serializers.CharField(read_only=True)


class LanguageSerializer(serializers.Serializer):
    Langauge_id = serializers.IntegerField(read_only=True)
    Description = serializers.CharField(read_only=True)


class MainInfoSerializer(serializers.Serializer):
    # use Serializer with announce fields
    Name = serializers.CharField(read_only=True)
    Surname = serializers.CharField(read_only=True)
    Age = serializers.IntegerField(read_only=True)
    Email = serializers.EmailField(read_only=True)
    Phone = serializers.CharField(read_only=True)
    Location = serializers.CharField(read_only=True)
    Competence = CompetenceSerializer(many=True, read_only=True)
    Language = LanguageSerializer(many=True, read_only=True)

    class Meta:
        model = MainInfoModel
        fields = '__all_'
        read_only_fields = '__all__'

    def create(self, validated_data):
        raise serializers.ValidationError(AllMessage.not_create)


class PhotoSerializer(serializers.Serializer):
    # use ModelSerializer unannounced fields
    Photo = serializers.CharField(read_only=True)

    class Meta:
        model = PhotoModel
        fields = '__all_'
        read_only_fields = '__all__'

    def create(self, validated_data):
        raise serializers.ValidationError(AllMessage.not_create)


class StudySerializer(serializers.Serializer):
    Organization = serializers.CharField(read_only=True)
    Start_date = serializers.DateField(read_only=True)
    End_date = serializers.DateField(read_only=True)
    Specialty = serializers.CharField(read_only=True)

    class Meta:
        model = StudyModel
        fields = '__all_'
        read_only_fields = '__all__'

    def create(self, validated_data):
        raise serializers.ValidationError(AllMessage.not_create)


class ResponsibilitySerializer(serializers.Serializer):
    Responsibility_id = serializers.IntegerField(read_only=True)
    Description = serializers.CharField(read_only=True)


class ProgressSerializer(serializers.Serializer):
    Progress_id = serializers.IntegerField(read_only=True)
    Description = serializers.CharField(read_only=True)


class WorkSerializer(serializers.Serializer):
    Organization = serializers.CharField(read_only=True)
    Start_date = serializers.DateField(read_only=True)
    End_date = serializers.DateField(read_only=True)
    Position = serializers.CharField(read_only=True)
    Responsibility = ResponsibilitySerializer(many=True, read_only=True)
    Progress = ProgressSerializer(many=True, read_only=True)

    class Meta:
        model = WorkModel
        fields = '__all_'
        read_only_fields = '__all__'

    def create(self, validated_data):
        raise serializers.ValidationError(AllMessage.not_create)


class ConsoleCommandsSerialaizer(serializers.Serializer):
    Command = serializers.CharField(read_only=True)
    Description = serializers.CharField(read_only=True)

    class Meta:
        model = ConsoleCommands
        fields = '__all__'
        read_only_fields = '__all__'

