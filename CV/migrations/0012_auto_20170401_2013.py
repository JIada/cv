# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-01 20:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CV', '0011_auto_20170401_1014'),
    ]

    operations = [
        migrations.CreateModel(
            name='EducationModel',
            fields=[
                ('Description', models.CharField(max_length=1500)),
                ('Education_id', models.AutoField(primary_key=True, serialize=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='maininfomodel',
            name='Education',
            field=models.ManyToManyField(db_table='education_info', to='CV.EducationModel'),
        ),
    ]
