# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-03-31 21:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CV', '0008_auto_20170103_1914'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConsoleCommands',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('command', models.CharField(max_length=30)),
                ('description', models.CharField(max_length=300)),
            ],
        ),
    ]
