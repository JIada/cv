from rest_framework import viewsets
from CV.models import MainInfoModel, PhotoModel, WorkModel, StudyModel, ConsoleCommands
from CV.serializer import (
    MainInfoSerializer, PhotoSerializer, WorkSerializer, StudySerializer, ConsoleCommandsSerialaizer
)

class MainInfoViewSet(viewsets.ModelViewSet):
    queryset = MainInfoModel.objects.all()
    serializer_class = MainInfoSerializer


class PhotoViewSet(viewsets.ModelViewSet):
    queryset = PhotoModel.objects.all()
    serializer_class = PhotoSerializer


class WorkViewSet(viewsets.ModelViewSet):
    queryset = WorkModel.objects.all()
    serializer_class = WorkSerializer


class StudyViewSet(viewsets.ModelViewSet):
    queryset = StudyModel.objects.all()
    serializer_class = StudySerializer


class ConsoleCommandsViewSet(viewsets.ModelViewSet):
    queryset = ConsoleCommands.objects.all()
    serializer_class = ConsoleCommandsSerialaizer
