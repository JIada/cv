from django.db import models


class DescriptionModel(models.Model):
    Description = models.CharField(max_length=1500)

    def __str__(self):
        return self.Description

    class Meta:
        abstract = True


class CompetenceModel(DescriptionModel):
    Competence_id = models.AutoField(primary_key=True)


class LanguageModel(DescriptionModel):
    Education_id = models.AutoField(primary_key=True)


class MainInfoModel(models.Model):
    Name = models.CharField(max_length=50)
    Surname = models.CharField(max_length=50)
    Age = models.PositiveSmallIntegerField()
    Email = models.EmailField()
    Phone = models.CharField(max_length=50)
    Location = models.CharField(max_length=70)
    Competence = models.ManyToManyField(CompetenceModel, db_table='competence_info')
    Language = models.ManyToManyField(LanguageModel, db_table='language_info')

    def __str__(self):
        return self.Name


class PhotoModel(models.Model):
    Photo = models.CharField(max_length=5000)


class InfoOrganiztionModel(models.Model):
    Organization = models.CharField(max_length=150)
    Start_date = models.DateField()
    End_date = models.DateField()

    def __str__(self):
        return self.Organization

    class Meta:
        abstract = True


class StudyModel(InfoOrganiztionModel):
    Specialty = models.CharField(max_length=150)


class ResponsibilityModel(DescriptionModel):
    Responsibility_id = models.AutoField(primary_key=True)


class ProgressModel(DescriptionModel):
    Progress_id = models.AutoField(primary_key=True)


class WorkModel(InfoOrganiztionModel):
    Position = models.CharField(max_length=150)
    Responsibility = models.ManyToManyField(ResponsibilityModel, db_table='responsibility_info')
    Progress = models.ManyToManyField(ProgressModel, db_table='progress_info')


class ConsoleCommands(models.Model):
    Command = models.CharField(max_length=30)
    Description = models.CharField(max_length=300)

    def __str__(self):
        return self.Command

    class Meta:
        ordering = ['Command']

















