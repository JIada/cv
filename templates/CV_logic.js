var HOST = "http://127.0.0.1:8000/";
var main_url = HOST + "main_info/";
var help_url = HOST + "console_commands/";
var work_url = HOST + "work/";
var study_url = HOST + "study/";
var photo_url = HOST + "photo/";
var dict = {
  "main" : main_url,
  "-m" : main_url,
  "--main" : main_url,
  "--help": help_url,
  "-h": help_url,
  "help": help_url,
  "-s": study_url,
  "--study": study_url,
  "study": study_url,
  "-w": work_url,
  "--work": work_url,
  "work": work_url,
  "-p": photo_url,
  "--photo": photo_url,
  "photo": photo_url
};

var start_row = "<div class='row'><div class='col-md-4' style='color: #04c72a'>";
var start_with_tab = "<div class='row'><div class='col-md-4' style='color: #04c72a; padding-left:3em'>";
var end_row = "</div></div>";
var start_ul = "<div class='row'><div class='col-md-5'><ul>";
var end_ul = "</ul></div></div>";
var start_li = "<li style='color: #04c72a; padding-left:3em'>";
var end_li = "</li>";
var row_padding = "<div class='row' style='padding: 5px'></div>";

function parse_json(data, key_input) {

        if (key_input === '--help' || key_input === '-h' || key_input === 'help')
    {
        return_help(data);
    }

        if (key_input === '--main' || key_input === '-m' || key_input === 'main')
    {
        return_main(data);
    }

        if (key_input === '--study' || key_input === '-s' || key_input === 'study')
    {
        return_study(data);
    }

        if (key_input === '--work' || key_input === '-w' || key_input === 'work')
    {
        return_work(data);
    }

        if (key_input === '--photo' || key_input === '-p' || key_input === 'photo')
    {
        return_photo(data);
    }

        if (key_input === '--code' || key_input === '-c' || key_input === 'code')
    {
        return_photo(data);
    }

}

function get_json(get_url, key_input){
    $.ajax({
        dataType: "json",
        url: get_url
        }).done(function ( data ) {
            parse_json(data, key_input);
    })
}

$(document).ready(function(){
    /**get data after click Enter**/
    $('#command').keyup(function(e){
    if(e.keyCode == 13)
        {
            $("#output_text").append("<p style='font-size:20pt' class='style_text'><b>> </b> " + $(this).val() + "</p>");
            var key_input = $(this).val();
            if (dict[key_input]){
               get_json(dict[key_input], key_input);

            }
            $('#command').val('')
        }
    });
});

function return_help(data) {
    $("#output_text").append(start_row + "Usage:" + end_row +
        start_with_tab + "[command]" + end_row +
        start_row + "Commands:" + end_row);
    $.each(data, function (key, value) {
        $("#output_text").append(
            start_with_tab + value['Command'] + "</div>" +
            "<div class='col-md-4' style='color: #04c72a; padding-left:3em'>" + value["Description"]+ end_row)
    })

}

function return_main(data){
    data = data[0];
    var main_info = start_row + data['Name'] + " " + data['Surname'] + end_row +
        start_row + data['Location'] + ", " + data['Age'] + " years old" + end_row +
        start_row + "Email: " + data['Email'] + end_row +
        start_row + "Phone: " + data['Phone'] + end_row;
    add_text(main_info);
    sub_list_main('Competence', data);
    sub_list_main('Language', data);
}

function return_study(data) {
    var main_info = start_row + "Education:" + end_row;
    add_text(main_info);
    var length_study = data.length;
    for (i = 0; i < length_study; i++) {
        var list_obj_content = row_padding +
        start_with_tab + data[i]['Organization'] + end_row +
        start_with_tab + data[i]['Start_date'] + end_row +
        start_with_tab + data[i]['End_date'] + end_row +
        start_with_tab + data[i]['Specialty'] + end_row;
        add_text(list_obj_content)
    }
}

function return_work(data) {
    var main_info = start_row + "Work:" + end_row;
    add_text(main_info);
    var length_work = data.length;
    for (i = 0; i < length_work; i++) {
        var work_obj_content = row_padding +
        start_with_tab + data[i]['Organization'] + end_row +
        start_with_tab + data[i]['Start_date'] + " / " + data[i]['End_date'] + end_row +
        start_with_tab + data[i]['Position'] + end_row;
        add_text(work_obj_content);
        sub_list_work(i, 'Responsibility', data);
        sub_list_work(i, 'Progress', data);
    }
}

function return_photo() {

}

function sub_list_work(i, sub_menu_name, data) {
        var length_obj = data[i][sub_menu_name].length;
        var start = start_with_tab + sub_menu_name + ":" + end_row + start_ul;
        add_text(start);
        for (z = 0; z < length_obj; z++) {
            var obj_content = start_li + data[i][sub_menu_name][z]['Description'] + end_li
            add_text(obj_content)
        }
        add_text(end_ul)
}

function sub_list_main(sub_menu_name, data) {
        add_text(start_row + sub_menu_name + ":" + end_row);
        var length_competence = data[sub_menu_name].length;
        add_text(start_ul);
        var list_obj_content;
        for (i = 0; i < length_competence; i++) {
            list_obj_content = start_li + data[sub_menu_name][i]['Description'] + end_li
            add_text(list_obj_content)
        }
        add_text(end_ul);
}

function add_text(text_for_add) {
    $("#output_text").append(text_for_add)
}

setInterval(function(){
    var focusbox;
    focusbox = document.getElementById("command");
    focusbox.focus();
});



